
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');

    for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') {
    c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
    return c.substring(name.length,c.length);
            }
    }

    return "";
}

if (!getCookie('isSigIn')){
    if(getCookie('role') == 'manager'){
        window.location.href="login-manager.html";
    }
        window.location.href="login-operator.html";
}

function removeAllCookies() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var pos = cookie.indexOf("=");
        var name = pos > -1 ? cookie.substr(0, pos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT;";
    }
}


function signOut() {

    var url = "login-operator.html";

    if(getCookie('role') == 'manager'){
        url = "login-manager.html";
    }

    removeAllCookies();


    // var auth2 = gapi.auth2.getAuthInstance();

    // console.log(auth2.isSignedIn.get());
    // auth2.signOut().then(function () {
    //   console.log('User signed out.');
    // });
    window.location.href=url;
    console.log(document.cookie);
}
